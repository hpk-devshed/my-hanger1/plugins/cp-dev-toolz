<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://hyperclock.eu
 * @since             1.0.0
 * @package           Cp_Dev_Toolz
 *
 * @wordpress-plugin
 * Plugin Name:       CP Dev Toolz
 * Plugin URI:        https://werxlab.com/downloads/
 * Description:       CP Dev Toolz is a sort of toolbox for developing a site using ClassicPress. It contains many small things that are missing on the core system. See thr werXlab site for detailed information.
 * Version:           1.0.0
 * Author:            JMColeman (hyperclock)
 * Author URI:        https://hyperclock.eu
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cp-dev-toolz
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CP_DEV_TOOLZ_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cp-dev-toolz-activator.php
 */
function activate_cp_dev_toolz() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cp-dev-toolz-activator.php';
	Cp_Dev_Toolz_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cp-dev-toolz-deactivator.php
 */
function deactivate_cp_dev_toolz() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cp-dev-toolz-deactivator.php';
	Cp_Dev_Toolz_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cp_dev_toolz' );
register_deactivation_hook( __FILE__, 'deactivate_cp_dev_toolz' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cp-dev-toolz.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cp_dev_toolz() {

	$plugin = new Cp_Dev_Toolz();
	$plugin->run();

}
run_cp_dev_toolz();
