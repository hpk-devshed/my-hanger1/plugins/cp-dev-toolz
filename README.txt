=== Plugin Name ===
Contributors: hyperclock
Donate link: https://hyperclock.eu
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

CP Dev Toolz is a sort of toolbox for developing a site using ClassicPress. It contains many small things that are missing on the core system. See thr werXlab site for detailed information.

