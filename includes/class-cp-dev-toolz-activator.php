<?php

/**
 * Fired during plugin activation
 *
 * @link       https://hyperclock.eu
 * @since      1.0.0
 *
 * @package    Cp_Dev_Toolz
 * @subpackage Cp_Dev_Toolz/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cp_Dev_Toolz
 * @subpackage Cp_Dev_Toolz/includes
 * @author     JMColeman (hyperclock) <hyperclock@werxlab.com>
 */
class Cp_Dev_Toolz_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
