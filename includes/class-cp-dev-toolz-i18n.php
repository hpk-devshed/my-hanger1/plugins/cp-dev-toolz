<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://hyperclock.eu
 * @since      1.0.0
 *
 * @package    Cp_Dev_Toolz
 * @subpackage Cp_Dev_Toolz/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Cp_Dev_Toolz
 * @subpackage Cp_Dev_Toolz/includes
 * @author     JMColeman (hyperclock) <hyperclock@werxlab.com>
 */
class Cp_Dev_Toolz_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'cp-dev-toolz',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
